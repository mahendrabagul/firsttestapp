package greet

import "fmt"

//Hello Function
func Hello(name string) string {
	return fmt.Sprintf("Hello, %s!", name)
}
