package main

import (
	"fmt"

	"gitlab.com/mahendrabagul/firsttestapp/greet"
)

func main() {
	fmt.Println("Running the FirstTestApp")
	fmt.Println(greet.Hello("Mahendra"))
}
